/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

/**
 *
 * @author fabio
 */
public class TestAutomovel {

    public static void main(String[] args) {
        //a
        Automovel a1 = new Automovel("11-11-AA", "Toyota", 1400);
        //b
        System.out.println(a1.toString());
        //c
        System.out.println(a1.getMatricula());
        //d
        a1.setCilindrada(1800);
        //e
        System.out.println(a1);
        //f
        System.out.println("Automoveis existentes: " + Automovel.getnAutomoveis());
        //g
        Automovel a2 = new Automovel();
        a2.setMarca("Audi");
        a2.setMatricula("22-22-BB");
        //h
        System.out.println(a2);
        //i
        System.out.println("Automoveis existentes: " + Automovel.getnAutomoveis());
        //j
        a2.setCilindrada(-2000);
        //k
        System.out.println(a2);
        //l
        a2.setCilindrada(2000);
        //m
        System.out.println(a2);
        //n
        System.out.println("Diferença de cilindrada entre a1 e a2: "
                + a1.diferenca(a2));
        //o
        System.out.print("Matrícula do automovel com mais cilindrada: ");
        if (a1.isSuperior(a2))
            System.out.println(a1.getMatricula() + ".");
        else
            System.out.println(a2.getMatricula() + ".");
    }
}
