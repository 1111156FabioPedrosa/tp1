/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

/**
 *
 * @author fabio
 */
public class Automovel {

    private String matricula;
    private String marca;
    private int cilindrada;
    private static int nAutomoveis = 0;

    Automovel() {
        matricula = "sem matrícula";
        marca = "sem marca";
        cilindrada = 1000;
        nAutomoveis++;
    }

    public Automovel(String matricula, String marca, int cilindrada) {
        this.matricula = matricula;
        this.marca = marca;
        this.cilindrada = cilindrada;
        nAutomoveis++;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getMarca() {
        return marca;
    }

    public int getCilindrada() {
        return cilindrada;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setCilindrada(int cilindrada) {
        if (cilindrada > 0) this.cilindrada = cilindrada;
    }

    public int diferenca(Automovel aut) {
        return Math.abs(cilindrada - aut.getCilindrada());
    }

    public boolean isSuperior(Automovel aut) {
        if (cilindrada > aut.getCilindrada()) {
            return true;
        } else {
            return false;
        }
    }

    public static int getnAutomoveis() {
        return nAutomoveis;
    }

    @Override
    public String toString() {
        return "Automóvel com matrícula " + matricula + " é um " + marca
                + " e tem cilindrada de " + cilindrada + " cc.";
    }
}
